// pages/api/process-image.js
import multer from "multer";
import { v2 as cloudinary } from "cloudinary";
import streamifier from "streamifier";

// Configure Cloudinary with your credentials
cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
    secure: true,
});

// Set up multer to store files in memory
const storage = multer.memoryStorage();
const upload = multer({ storage });
const uploadMiddleware = upload.single("file");

export default async function handler(req, res) {
    // Run the upload middleware to parse the file from the request
    uploadMiddleware(req, res, (err) => {
        if (err) {
            return res.status(500).json({ error: 'Error uploading file.' });
        }

        // Ensure a file was uploaded
        if (!req.file) {
            return res.status(400).json({ error: 'No file uploaded.' });
        }

        // Create a Cloudinary upload stream
        const stream = cloudinary.uploader.upload_stream(
            {
                resource_type: 'image', transformation: [
                    { width: 500, crop: "scale" },
                    { fetch_format: "auto", quality: "auto" }
                ]
            },
            (error, result) => {
                if (error) {
                    console.error(error);
                    return res.status(500).json({ error: 'Error uploading to Cloudinary.' });
                }
                // Respond with the URL of the uploaded image
                console.log({ secure_url: result.secure_url, id: result.asset_id })
                res.status(200).json({ imageUrl: result.secure_url });
            }
        );

        // Use streamifier to create a readable stream from the buffer and pipe it to Cloudinary
        streamifier.createReadStream(req.file.buffer).pipe(stream);
    });
}

export const config = {
    api: {
        bodyParser: false,
    },
};