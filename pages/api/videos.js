import Redis from "ioredis";

const redisClient = new Redis("rediss://default:9a9235710b084378abadbd6ffae2ecb9@eu2-tender-grackle-32047.upstash.io:32047");
const pageSize = 6;

export default async function handler(req, res) {
    const { page } = req.query;
    const pageIndex = parseInt(page, 10) || 1;

    try {
        const start = (pageIndex - 1) * pageSize;
        const end = start + pageSize - 1;

        // Retrieve a range of video JSON strings from the sorted set with pagination
        const videoJsonStrings = await redisClient.zrevrange('userVideos', start, end);

        // Parse the JSON strings into objects
        const videos = videoJsonStrings.map(jsonString => JSON.parse(jsonString));

        res.status(200).json({
            page: pageIndex,
            pageSize: pageSize,
            videos: videos
        });
    } catch (error) {
        console.error('Error retrieving videos:', error);
        res.status(error.response?.status || 500).json({ message: 'Error retrieving videos: ' + error.message });
    }
}