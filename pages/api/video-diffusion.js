// pages/api/video-diffusion.js
import Replicate from "replicate";
import Redis from "ioredis";
import cloudinary from 'cloudinary';
import axios from 'axios';

// Configure Cloudinary with your credentials
cloudinary.v2.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

// Initialize Redis client
const redisClient = new Redis("rediss://default:9a9235710b084378abadbd6ffae2ecb9@eu2-tender-grackle-32047.upstash.io:32047");
const videosSetKey = 'userVideos';

export default async function handler(req, res) {
  if (req.method !== 'POST') {
    res.setHeader('Allow', ['POST']);
    return res.status(405).end(`Method ${req.method} Not Allowed`);
  }

  const { imageUrl } = req.body;
  try {
    const replicate = new Replicate({
      token: process.env.REPLICATE_API_TOKEN,
    });
    const output = await replicate.run(
      "stability-ai/stable-video-diffusion:3f0457e4619daac51203dedb472816fd4af51f3149fa7a9e0b5ffcf1b8172438",
      {
        input: {
          cond_aug: 0.12,
          decoding_t: 14,
          input_image: imageUrl,
          video_length: "25_frames_with_svd_xt",
          sizing_strategy: "maintain_aspect_ratio",
          motion_bucket_id: 127,
          frames_per_second: 15
        }
      }
    );

    // Assuming `output` is a URL to the generated video
    // Download the video from the output URL
    const videoResponse = await axios.get(output, { responseType: 'arraybuffer' });
    const videoBuffer = Buffer.from(videoResponse.data);

    // Upload the video to Cloudinary
    const uploadResponse = await new Promise((resolve, reject) => {
      cloudinary.v2.uploader.upload_stream({ resource_type: 'video' }, (error, result) => {
        if (error) reject(error);
        else resolve(result);
      }).end(videoBuffer);
    });

    // Store the Cloudinary URL of the uploaded video in Redis
    const videoData = { url: uploadResponse.secure_url, image: imageUrl };
    const timestamp = Date.now();
    await redisClient.zadd(videosSetKey, timestamp, JSON.stringify(videoData));

    res.status(200).json(videoData);
  } catch (error) {
    console.error(error);
    res.status(error.response?.status || 500).json({ message: 'Error generating video: ' + error.message });
  }
}