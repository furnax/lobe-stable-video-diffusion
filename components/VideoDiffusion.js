// components/VideoDiffusion.js
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Lottie from 'lottie-react';
import loadingAnimation from '../public/loading.json';
import VideoGallery from './VideoGallery';
import ImageUploadModal from './ImageUploadModal'; // Import the ImageUploadModal component

export default function VideoDiffusion() {
  const [imageUrl, setImageUrl] = useState('');
  const [videoResult, setVideoResult] = useState(null);
  const [loading, setLoading] = useState(false);
  const [secondsElapsed, setSecondsElapsed] = useState(0);
  const [isModalOpen, setIsModalOpen] = useState(false); // State to control modal visibility

  useEffect(() => {
    let intervalId;

    if (loading) {
      intervalId = setInterval(() => {
        setSecondsElapsed((prevSeconds) => prevSeconds + 1);
      }, 1000);
    }

    return () => {
      clearInterval(intervalId);
    };
  }, [loading]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      const response = await axios.post('/api/video-diffusion', { imageUrl });
      setVideoResult(response.data);
    } catch (error) {
      console.error('Error generating video:', error);
    } finally {
      setLoading(false);
    }
  };

  const handleImageReady = (url) => {
    setImageUrl(url); // Set the image URL after upload is complete
    handleSubmit(new Event('submit')); // Trigger video generation
  };

  return (
    <div className="container mx-auto p-4 flex flex-col items-center">
      <div className="form-control max-w-xs">
        <div className="join">
          <input
            id="imageUrl"
            type="text"
            value={imageUrl}
            onChange={(e) => setImageUrl(e.target.value)}
            placeholder="Enter an image URL"
            className="input input-bordered  join-item"
          />
          <button
            type="button"
            onClick={handleSubmit}
            disabled={loading}
            className="btn btn-square btn-primary join-item"
          >
            {loading ? (
              <Lottie animationData={loadingAnimation} loop={true} className="w-6 h-6" />
            ) : (
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M12.75 15l3-3m0 0l-3-3m3 3h-7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
              </svg>
            )}
          </button>
        </div>
        <button
          type="button"
          onClick={() => setIsModalOpen(true)}
          className="btn btn-secondary mt-4"
          disabled={loading}
        >
          Or upload <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={3} stroke="currentColor" className="w-4 h-4">
  <path strokeLinecap="round" strokeLinejoin="round" d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5" />
</svg>

        </button>
      </div>

      {videoResult && !loading && (
        <div className="mt-4">
          <p className="text-sm text-gray-700">Video generation completed in {secondsElapsed} seconds:</p>
          <video src={videoResult.url} autoPlay loop controls className="w-full max-w-lg" />
        </div>
      )}

      <VideoGallery />

      {/* Include the ImageUploadModal component */}
      <ImageUploadModal isOpen={isModalOpen} onClose={() => setIsModalOpen(false)} onImageReady={handleImageReady} />
    </div>
  );
}