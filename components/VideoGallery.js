import { useState, useEffect } from 'react';

const VideoGallery = () => {
    const [videos, setVideos] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [modalContent, setModalContent] = useState(null);
    const [isModalOpen, setIsModalOpen] = useState(false);

    useEffect(() => {
        const fetchVideos = async () => {
            console.log('Fetching videos for page:', currentPage);
            try {
                const response = await fetch(`/api/videos?page=${currentPage}`);
                const data = await response.json();
                console.log('Data received:', data);
                if (data && data.videos && Array.isArray(data.videos)) {
                    setVideos((prevVideos) => [...prevVideos, ...data.videos]);
                }
            } catch (error) {
                console.error('Fetching error:', error);
            }
        };

        fetchVideos();
    }, [currentPage]);

    const handleLoadMore = () => {
        setCurrentPage((prevPage) => prevPage + 1);
    };

    const openModal = (video) => {
        setModalContent(video.url);
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    return (
        <>
            <div className="container mx-auto p-4 items-center">
                <div className="grid grid-cols-3 gap-4">
                    {videos.map((video, index) => (
                        <div key={index} className="aspect-w-16 aspect-h-9 bg-cover bg-center cursor-pointer rounded overflow-hidden" onClick={() => openModal(video)}>
                            {/* Video element as background */}
                            <video className="w-full h-full object-cover" autoPlay loop muted playsInline>
                                <source src={video.url} type="video/mp4" />
                            </video>
                        </div>
                    ))}
                </div>
                <center>
                    <button onClick={handleLoadMore} className="btn btn-outline btn-secondary mt-4">
                        Load More
                    </button>
                </center>
            </div>

            {isModalOpen && (
                <div className={`modal ${isModalOpen ? 'modal-open' : ''}`}>
                    <div className="modal-box relative">
                        <label htmlFor="my-modal" className="btn btn-sm btn-circle absolute right-2 top-2" onClick={closeModal}>✕</label>
                        <video className="w-full" controls autoPlay loop>
                            <source src={modalContent} type="video/mp4" />
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            )}
        </>
    );
};

export default VideoGallery;