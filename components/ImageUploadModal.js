// components/ImageUploadModal.js
import React, { useCallback, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import axios from 'axios';

const ImageUploadModal = ({ isOpen, onClose, onImageReady }) => {
  const [uploading, setUploading] = useState(false);

  const onDrop = useCallback(async (acceptedFiles) => {
    const file = acceptedFiles[0];
    setUploading(true);

    try {
      const formData = new FormData();
      formData.append('file', file);

      const response = await axios.post('/api/process-image', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      console.log({received_by_modal: response, rdata: response.data})

      onImageReady(response.data.imageUrl);
    } catch (error) {
      console.error('Error uploading image:', error);
    } finally {
      setUploading(false);
      onClose();
    }
  }, [onClose, onImageReady]);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: 'image/*',
    // maxSize: 512 * 1024, // 512KB max file size
  });

  return (
    <div className={`modal ${isOpen ? 'modal-open' : ''}`}>
      <div className="modal-box">
        <h3 className="font-bold text-lg">Upload Image</h3>
        <div {...getRootProps()} className={`p-4 mt-2 200 ${isDragActive ? 'bg-neutral-content' : 'bg-base-200'}`}>
          <input {...getInputProps()} />
          {uploading ? (
            <p>Uploading...</p>
          ) : (
            <p>Drag and drop an image here, or tap to select one</p>
          )}
        </div>
        <div className="modal-action">
          <button className="btn" onClick={onClose}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

export default ImageUploadModal;